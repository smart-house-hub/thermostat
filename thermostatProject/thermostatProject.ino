/**
    This file is part of Smart House - Thermostat.

    Smart House - Thermostat is free software: you can redistribute it and/or
    modify it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Smart House - Thermostat is distributed in the hope that it will be
    useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*/

/*********************************************************************
  # DOCUMENTATION

  ## Instaling libraries:
  In arduino IDE go to manage libraries and install:
  - Adafruit SSD1306 2.2.1  - make sure you accept the other dependencies for this Library
  - EnableInterrupt 1.1.0
  - neotimer 1.1.6
  - the other should be instaled either by default either by dependencies. if something is missing install manualy the required version

  ## Console codes explained:
  If you notice in terminal strage caracters that start with a # in front it means it might be either a debug message or an error.
  If char `#` is folloed by a `D` then it is a debug message, operations are normal, if the char `#` is followed by an `E` then the board is signaling an error

  Consult the below grafic for debuging the error.
    `#D_1` - Maintaining ethernet
    `#D_2` - RENEWD SUCCESS
    `#D_3` - CALLING CENTRAL BOX
    `#D_4` - CENTRAL BOX RECEIVED THE DATA
    `#E_1` - ETHERNET SETUP ERROR
    `#E_2` - CALLING CENTRAL BOX ERROR


  ## Other details
  Before runing program on linux: `sudo chmod a+rw /dev/ttyUSB0`

  # Refrences
    SSD1306-OLED example: http://robojax.com/learn/arduino/?vid=robojax-SSD1306-OLED-128x32
*********************************************************************/

// LCD Library
#include <SPI.h>
#include <Ethernet.h> // Ethernet version 2.0
#include <Adafruit_GFX.h> // version 1.8.3
#include <Adafruit_SSD1306.h> // version 2.2.1 https://github.com/adafruit/Adafruit_SSD1306
#include <EnableInterrupt.h> // version 1.1.0 Library: https://github.com/GreyGnome/EnableInterrupt
#include <neotimer.h> // version 1.1.6 Library: https://github.com/jrullan/neotimer

// Display static variables
#define OLED_RESET 1
Adafruit_SSD1306 display(OLED_RESET);

// Rotary Encoder Module connections
#define PIN_SW 2 // Our encoder button(when pressing the knob) is set for pin 5
#define PIN_CLK 5 // Rotary CLK pin connected to UNO interrupt 0 on pin 2
#define PIN_DT 6 // Rotary DT pin connected to UNO pin 4
#define SCREEN_ON_TIME 120000 // 2 minutes

// Ethernet variables
#define CENTRAL_BOX_PORT 1880
#define CENTRAL_BOX_SERVER F("central-box")

// Network variables
byte mac[] = { 0x9E, 0x13, 0x54, 0x4A, 0x20, 0x7D };
EthernetClient client;

// Defineing variables
Neotimer displayTimer = Neotimer(SCREEN_ON_TIME);
Neotimer ethernetTimer = Neotimer(SCREEN_ON_TIME);
volatile byte selectedTemperature = 18; // stores the last encoder position value so we can compare to the current encoderRawReading and see if it has changed (so we know when to print to the serial monitor)
volatile boolean enableDisplay = true;


void setup()   {
  Serial.begin(9600);

  // Oled LCD setup
  // by default, we'll generate the high voltage from the 3.3v line internally! (neat!)
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3D (for the 128x32)
  display.display();

  // Clear the buffer.
  display.clearDisplay();

  //Buttons + Rotary Encoder set up
  pinMode(PIN_CLK, INPUT); // Set normal 5 pin to Input
  pinMode(PIN_DT, INPUT); // Set normal pin6 to Input
  pinMode(PIN_SW, INPUT); // Set Pin 2 (Interrupt zero) to Input

  // Attaching hardware interupts
  //attachInterrupt (digitalPinToInterrupt(PIN_SW), encoderBtnPressed, FALLING); // interrupt 0 always connected to pin 2 on Arduino UNO
  enableInterrupt(PIN_SW, encoderBtnPressed, FALLING); // interrupt 0 always connected to pin 2 on Arduino UNO
  enableInterrupt(PIN_CLK, rotaryStateDetect, CHANGE); // interrupt connected to pin 11

  ethernetSetup();

  // Default values
  selectedTemperature = 10;
  displayTimer.start();
}

void ethernetSetup() {
  // Network setup

  // You can use Ethernet.init(pin) to configure the CS pin
  //Ethernet.init(10);  // Most Arduino shields
  //Ethernet.init(5);   // MKR ETH shield
  //Ethernet.init(0);   // Teensy 2.0
  //Ethernet.init(20);  // Teensy++ 2.0
  //Ethernet.init(15);  // ESP8266 with Adafruit Featherwing Ethernet
  //Ethernet.init(33);  // ESP32 with Adafruit Featherwing Ethernet

  // start the Ethernet connection:
  // Serial.println(F("Initialize Ethernet with DHCP:"));
  if (Ethernet.begin(mac) == 0) {
    // no point in carrying on, so do nothing forevermore:
    Serial.println(F("#E1"));
    return;
  } else {
    // print your local IP address:
    // Serial.print(F("IP: "));
    Serial.println(Ethernet.localIP());
  }
}

void encoderBtnPressed() {
  enableDisplay = !enableDisplay;
  if (enableDisplay) {
    displayTimer.reset();
    displayTimer.start();
  } else {
    displayTimer.stop();
  }
};

void disableLcd() {
  enableDisplay = false;
  updateDisplay();
  displayTimer.stop();
}

// Interrupt routine on Pin 2 (Interrupt zero)runs if CLK pin changes state
void rotaryStateDetect () {
  delay(1); // delay for Debouncing Rotary Encoder

  if (!displayEnabled()) {
    return;
  }

  if (digitalRead(PIN_CLK)) {
    if (digitalRead(PIN_DT)) {
      // decrement the encoder's position count
      selectedTemperature == 6 ? selectedTemperature = 6 : selectedTemperature--;
    }
    if (!digitalRead(PIN_DT)) {
      // increment the encoder's position count
      selectedTemperature == 30 ? selectedTemperature = 30 : selectedTemperature++;
    }
  }

  displayTimer.restart();
}


void loop() {
  ethernetLoop(); //enable for ethernet connection
  displayLoop();
  callCentralBox();
}

void ethernetLoop() {

  if (ethernetTimer.repeat()) {
    Serial.println(F("#D_1"));

    switch (Ethernet.maintain()) {
      case 1:
        // renewed fail
        Serial.println(F("#E1"));
        break;

      case 2:
        // renewed success
        Serial.println(F("#D_2"));
        // print your local IP address:
        // Serial.print(F("My IP address: "));
        Serial.println(Ethernet.localIP());
        break;

      case 3:
        // rebind fail
        Serial.println(F("#E1"));
        break;

      case 4:
        //rebind success
        // Serial.println(F("Rebind success"));
        // print your local IP address:
        // Serial.print(F("My IP address: "));
        Serial.println(Ethernet.localIP());
        break;

      default:
        // nothing happened
        break;
    }
    // Serial.println(F("Finish Maintaining ethernet"));
  }
}

void displayLoop() {
  if (displayTimer.done()) {
    disableLcd();
  }

  if (displayEnabled()) {
    prepareLCD();
  }

  updateDisplay();
}

void prepareLCD() {
  display.clearDisplay();
  display.setTextColor(WHITE);

  display.setCursor(0, 0);
  display.setTextSize(4);
  display.println(String(selectedTemperature));
  // Degree symbol
  display.setCursor(60, 0);
  display.setTextSize(2);
  display.println((char)247);

  // Fire place
  display.setCursor(100, 0);
  display.println((char)271);

  updateDisplay();
}

boolean displayEnabled() {
  return enableDisplay;
}

/**
   If the display is enabled then it will push the modifications to the display,
   otherwise it will turn it off
*/
void updateDisplay() {
  if (!displayEnabled()) {
    display.clearDisplay();
    display.display();
  } else {
    display.display();
  }
}

void callCentralBox() {

  Serial.println(F("#D_3"));

  char temperatureStr[5];

  // convert selectedTemperature to string
  itoa(selectedTemperature, temperatureStr, 10);

  char postData = F("{\"temperature\":");
  strcat(postData, temperatureStr);
  strcat(postData, "}");

  // if you get a connection, report back via serial:
  if (client.connect(CENTRAL_BOX_SERVER, CENTRAL_BOX_PORT)) {
    //Serial.print("connected to ");
    Serial.println(client.remoteIP());

    // Make a HTTP request:
    client.println(F("POST /thermostat/temperature HTTP/1.1"));

    client.print(F("Host: "));
    client.print(CENTRAL_BOX_SERVER);
    client.print(F(":"));
    client.println(CENTRAL_BOX_PORT);

    client.print(F("Authorization: Basic "));
    client.println(F("authorization"));
    client.println(F("Connection: close"));
    client.print(F("Content-Length: "));
    client.println(strlen(postData));
    client.println();
    client.println(postData);

  } else {
    // if you didn't get a connection to the server:
    Serial.println(F("#E_2"));
  }
  Serial.println(F("#D_4"));
}
