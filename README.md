# Overview

The [Smart House group](https://gitlab.com/smart-house-hub) contains multiple projects. One of them is the [Central Box](https://gitlab.com/smart-house-hub/central-box). All the projects add data to the Central Box or is triggered by the Central Box.

For this scenario the Thermostat project uses the Central Box as follows:
1. When first powering up(or restart occurs), it retrievs the 'wanted temperature' that was previously set.
2. Sends the current room temperature
3. Sends the 'wanted temperature' when user adjusts it from the encoder
4. Syncronizes the wanted temperat from the Central Box if the wanted temperature was set using the Dashboard UI

### Note
If temperature readings are needed for each individual room, then use the [Room Temperature Reporter](https://gitlab.com/smart-house-hub/room-temperature-reporter) **and do not use this(the Thermostat) project**.

# Thermostat project

## Description
The *Thermostat* project describes the parts needed, and shows a sample code of how a house thermostat could set the desired/needed temperature inside the house and also monitor the current temperature that is inside the house.

All the data is communicated to the [Central Box](https://gitlab.com/smart-house-hub/central-box) project. It's the Central Box's duty to call the [Boiller Controller](https://gitlab.com/smart-house-hub/boiler-controller) to turn on/off the heater when needed

## Prerequisites

* [Arduino Uno](https://www.banggood.com/UNO-R3-ATmega328P-Development-Board-For-Arduino-No-Cable-p-964163.html?rmmds=myorder&cur_warehouse=CN)
* [Arduino Ethernet Shield](https://www.banggood.com/Ethernet-Shield-W5100-R3-Support-PoE-For-Arduino-UNO-Mega-2560-Nano-p-907969.html?rmmds=myorder&cur_warehouse=CN)
* [Temperature Sensor](https://www.banggood.com/DS18B20-Temperature-Sensor-DALLAS-18B20-TO-92-Encapsulation-p-91798.html?rmmds=myorder&cur_warehouse=CN)
* [Encoder](https://www.banggood.com/KY-040-Rotary-Decoder-Encoder-Module-For-Arduino-AVR-PIC-p-914010.html?rmmds=search&cur_warehouse=CN)
* Display Oled 128x32 - I2C(important to be I2C) - [what I am using](https://ardushop.ro/ro/home/393-display-oled-128x32-i2c.html?search_query=OLED-12832-white&results=18) but can be any other that has I2C support
* 2 x 47kΩ resistor for Oled display

Note: For Arduino Uno and Ethernet Shield, I would recommand using the original parts as these are the cheap ones from china. Personally I did not have problems with them YET, but experience may very with this ones.

## Wiring

![Wiring](images/thermostat.png)

**Please note that VCC and GND are not connected to simplify the image**
**Use 3.3V for the Older display!**

## How the code works
TBC